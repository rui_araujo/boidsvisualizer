#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "fixedptc.h"
#include "vec.h"
#include "boid.h"

#define rrand(a) (rand()%a)

#define DEFAULT_CENTER_BIAS 7
#define DEFAULT_AVG_VEL     3
#define DEFAULT_CHILLING    1

Boid new_boid(int W, int H) {
	Boid boid;
	fixedpt px, py, pz;
	fixedpt vx, vy, vz;

	boid = (Boid) malloc(sizeof(_Boid));
	memset(boid, 0, sizeof(_Boid));
	px = fixedpt_fromint(rrand((W<<4)) - (W << 3));
	py = fixedpt_fromint(rrand((H<<4)) - (H << 3));
	pz = fixedpt_fromint(rrand(2000) + 2000);

	boid->pos = new_vec(px, py, pz);

	vx = fixedpt_fromint(rrand(51) - 25);
	vy = fixedpt_fromint(rrand(51) - 25);
	vz = fixedpt_fromint(rrand(51) - 25);

	boid->vel = new_vec(vx, vy, vz);

	boid->wing_level = rrand(200) - 100;

	boid_perspective(boid, W, H);

	return boid;
}

void boid_perspective(Boid boid, int W, int H) {
	fixedpt zfactor, zf;
	Vec tail, tail_end;
	fixedpt tail_lx, tail_lz, tail_rx, tail_rz;
	fixedpt tailx, tailz;

	tail = vec_copy(boid->vel);
	tail_end = vec_copy(boid->vel);

	if (boid->pos->z <= 0) {
		boid->onscreen = 0;
	} else {
		zf = fixedpt_div(fixedpt_fromint(W), fixedpt_rconst(2.5));
		zfactor = fixedpt_div(boid->pos->z, zf);

		boid->X = (W >> 1) + fixedpt_toint(fixedpt_div(boid->pos->x, zfactor));
		boid->Y = (H >> 1) + fixedpt_toint(fixedpt_div(boid->pos->y, zfactor));

		boid->shadow[0].x = boid->X;
		boid->shadow[0].y = (H >> 1)
				+ fixedpt_toint(fixedpt_div(fixedpt_fromint(1000) , zfactor));

		vec_setmag(tail_end, fixedpt_fromint(40));
		vec_diff(boid->pos, tail_end, tail_end);

		zfactor = fixedpt_div(tail_end->z, zf);
		boid->tail_X = (W >> 1)
				+ fixedpt_toint(fixedpt_div(tail_end->x, zfactor));
		boid->tail_Y = (H >> 1)
				+ fixedpt_toint(fixedpt_div(tail_end->y, zfactor));
		boid->shadow[2].x = boid->tail_X;
		boid->shadow[2].y = (H >> 1)
				+ fixedpt_toint(fixedpt_div(fixedpt_fromint(1000) , zfactor));

		vec_setmag(tail, fixedpt_fromint(50));
		vec_diff(boid->pos, tail, tail);

		tailx = fixedpt_div(-tail->z, fixedpt_fromint(60));
		tailz = fixedpt_div(tail->x, fixedpt_fromint(60));

		tail_lx = tail->x - tailx;
		tail_lz = tail->z - tailz;

		tail_rx = tail->x + tailx;
		tail_rz = tail->z + tailz;

		tail->y -= boid->wing_level;

		zfactor = fixedpt_div(tail_lz, zf);
		boid->tail_lX = (W >> 1) + fixedpt_toint(fixedpt_div(tail_lx, zfactor));
		boid->tail_lY = (H >> 1) + fixedpt_toint(fixedpt_div(tail->y, zfactor));
		boid->shadow[1].x = boid->tail_lX;
		boid->shadow[1].y = (H >> 1)
				+ fixedpt_toint(fixedpt_div(fixedpt_fromint(1000) , zfactor));

		zfactor = fixedpt_div(tail_rz, zf);
		boid->tail_rX = (W >> 1) + fixedpt_toint(fixedpt_div(tail_rx, zfactor));
		boid->tail_rY = (H >> 1) + fixedpt_toint(fixedpt_div(tail->y, zfactor));
		boid->shadow[3].x = boid->tail_rX;
		boid->shadow[3].y = (H >> 1)
				+ fixedpt_toint(fixedpt_div(fixedpt_fromint(1000) , zfactor));

		boid->onscreen = boid_isonscreen(boid, W, H);
	}

	free(tail);
	free(tail_end);
}

int boid_isonscreen(Boid boid, int W, int H) {
	return (boid->X >= 0 && boid->X < W && boid->Y >= 0 && boid->Y < H);
}

/*
 * Move this boid, wrt allboids
 */
void boid_move(Boid boid, Boid allboids[], int numboids, Vec real_center,
		Vec real_avgvel, int W, int H) {
	Vec center, center_bias;
	Vec avgvelocity, avgvel_bias;
	Vec chilling;

	if (boid->perching) {
		if (boid->perch_timer > 0) {
			boid->perch_timer--;
			return;
		} else {
			boid->perching = 0;
		}
	}

	center_bias = zero_vec();
	center = boid_perceive_center(boid, real_center, numboids);
	vec_diff(center, boid->pos, center_bias);
	vec_rshift(center_bias, DEFAULT_CENTER_BIAS);

	avgvel_bias = zero_vec();
	avgvelocity = boid_av_vel(boid, real_avgvel, numboids);
	vec_diff(avgvelocity, boid->vel, avgvel_bias);
	vec_rshift(avgvel_bias, DEFAULT_AVG_VEL);

	chilling = boid_chill_out(boid, allboids, numboids);
	vec_rshift(chilling, DEFAULT_CHILLING);

	vec_add(boid->vel, center_bias);
	vec_add(boid->vel, avgvel_bias);
	vec_add(boid->vel, chilling);

	vec_limit(boid->vel, fixedpt_fromint(100));

	vec_add(boid->pos, boid->vel);

	free(center);
	free(center_bias);
	free(avgvelocity);
	free(avgvel_bias);
	free(chilling);

	if (boid->upstroke) {
		if (boid->wing_level >= 100) {
			boid->upstroke = 0;
		} else {
			boid->wing_level += 40;
		}
	} else {
		if (boid->wing_level <= -100) {
			boid->upstroke = 1;
		} else {
			boid->wing_level -= 20;
		}

	}

	/* bound world */
	if (boid->pos->x < fixedpt_fromint(-1500)) {
		boid->vel->x += fixedpt_fromint(10);
	}
	if (boid->pos->x > fixedpt_fromint(1500)) {
		boid->vel->x -= fixedpt_fromint(10);
	}
	if (boid->pos->y < fixedpt_fromint(-1200)) {
		boid->vel->y += fixedpt_fromint(10);
	}
	if (boid->pos->y > fixedpt_fromint(800)) {
		boid->vel->y -= fixedpt_fromint(10);
	}
	if (boid->pos->y > fixedpt_fromint(1000)) {
		boid->pos->y = fixedpt_fromint(1000); /* Hit ground!! */
		boid->perching = 1;
		boid->perch_timer = (int) (rrand(20) + 30);
		boid->wing_level = 60;
		boid->vel->y = 0;
	}
	if (boid->pos->z < fixedpt_fromint(1500)) {
		boid->vel->z += fixedpt_fromint(10);
	}
	if (boid->pos->z > fixedpt_fromint(3000)) {
		boid->vel->z -= fixedpt_fromint(10);
	}

	boid_perspective(boid, W, H);
}

Vec boid_perceive_center(Boid boid, Vec real_cent, int numboids) {
	Vec perc_cent;

	perc_cent = zero_vec();
	vec_diff(real_cent, boid->pos, perc_cent);
	vec_sdiv(perc_cent, fixedpt_fromint(numboids - 1));

	return perc_cent;
}

Vec boid_av_vel(Boid boid, Vec real_avgvel, int numboids) {
	Vec perc_avgvel;

	perc_avgvel = zero_vec();
	vec_diff(real_avgvel, boid->vel, perc_avgvel);
	vec_sdiv(perc_avgvel, fixedpt_fromint(numboids - 1));

	return perc_avgvel;
}

Vec boid_chill_out(Boid boid, Boid boids[], int numboids) {
	Vec chill, bigchill;
	int i;

	chill = zero_vec();
	bigchill = zero_vec();

	for (i = 0; i < numboids; i++) {
		if (boids[i] != boid) {
			if (vec_rdist(boid->pos, boids[i]->pos) <= fixedpt_fromint(100)) {
				vec_diff(boid->pos, boids[i]->pos, chill);
				vec_add(bigchill, chill);
			}
		}
	}

	free(chill);

	return bigchill;
}

