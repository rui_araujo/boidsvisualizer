package de.tum.BoidsVisualizer;

import java.util.Random;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class Boid {
	private static final Random RANDOM = new Random(System.currentTimeMillis());

	private static final int DEFAULT_CENTER_BIAS = 7;
	private static final int DEFAULT_AVG_VEL = 1;
	private static final int DEFAULT_CHILLING = 1;
	private static final int FLOCK_AVG_VEL = 6;
//	private static final int FLOCK_AVOIDANCE = 9;
//	private static final int FLOCK_CENTER_BIAS = 14;

	private static int rrand(int a) {
		return Math.abs(RANDOM.nextInt()) % a;
	}

	// Drawing objects
	private final Vector3 lwing[];
	private final Vector3 rwing[];

	public Vector3 pos;
	public Vector3 vel;
	private int X;
	private int Y;
	private int tail_lX;
	private int tail_lY;
	private int tail_rX;
	private int tail_rY;
	private int tail_X;
	private int tail_Y;
	private final Vector2 shadow[];
	private int wing_level;
	private boolean onscreen;
	private boolean upstroke;
	private boolean perching;
	private int perch_timer;
	private final Color color;

	public Boid(int width, int height, Color boidsColors) {
		pos = new Vector3(rrand((width << 4)) - (width << 3),
				rrand((height << 4)) - (height << 3), rrand(2000) + 3000);
		vel = new Vector3(rrand(51) - 25, rrand(51) - 25, rrand(51) - 25);
		wing_level = rrand(200) - 100;
		lwing = new Vector3[3];
		for (int i = 0; i < lwing.length; ++i)
			lwing[i] = new Vector3();
		rwing = new Vector3[3];
		for (int i = 0; i < rwing.length; ++i)
			rwing[i] = new Vector3();
		shadow = new Vector2[4];
		for (int i = 0; i < shadow.length; ++i) {
			shadow[i] = new Vector2();
		}
		this.color = boidsColors;
		perspective(width, height);
	}

	private final Vector3 tail = new Vector3();
	private final Vector3 tailEnd = new Vector3();

	private void perspective(int width, int height) {
		double zfactor, zf;
		double tail_lx, tail_lz, tail_rx, tail_rz;
		double tailx, tailz;
		tail.set(vel);
		tailEnd.set(vel);

		if (pos.z <= 0) {
			onscreen = false;
		} else {
			zf = width / (double) 2;
			zfactor = ((double) pos.z) / zf;

			X = (width >> 1) + (int) (pos.x / zfactor);
			Y = (height >> 1) + (int) (pos.y / zfactor);

			shadow[0].x = X;
			shadow[0].y = (height >> 1) + (int) (1000 / zfactor);

			vector3SetMag(tailEnd, 40);
			tailEnd.set(pos.x - tailEnd.x, pos.y - tailEnd.y, pos.z - tailEnd.z);

			zfactor = ((double) tailEnd.z) / zf;
			tail_X = (width >> 1) + (int) (tailEnd.x / zfactor);
			tail_Y = (height >> 1) + (int) (tailEnd.y / zfactor);
			shadow[2].x = tail_X;
			shadow[2].y = (height >> 1) + (int) (1000 / zfactor);

			vector3SetMag(tail, 50);
			tail.set(pos.x - tail.x, pos.y - tail.y, pos.z - tail.z);

			tailx = -tail.z / 60;
			tailz = tail.x / 60;

			tail_lx = tail.x - tailx;
			tail_lz = tail.z - tailz;

			tail_rx = tail.x + tailx;
			tail_rz = tail.z + tailz;

			tail.y -= wing_level;

			zfactor = ((double) tail_lz) / zf;
			tail_lX = (width >> 1) + (int) (tail_lx / zfactor);
			tail_lY = (height >> 1) + (int) (tail.y / zfactor);
			shadow[1].x = tail_lX;
			shadow[1].y = (height >> 1) + (int) (1000 / zfactor);

			zfactor = ((double) tail_rz) / zf;
			tail_rX = (width >> 1) + (int) (tail_rx / zfactor);
			tail_rY = (height >> 1) + (int) (tail.y / zfactor);
			shadow[3].x = tail_rX;
			shadow[3].y = (height >> 1) + (int) (1000 / zfactor);

			onscreen = isOnScreen(width, height);
		}
	}

	/*
	 * Move this boid, wrt allboids
	 */

	// private final Vector3 scaledPos = new Vector3();
	// private final Vector3 centerFlock = new Vector3();
	// private final Vector3 flockAvoidance = new Vector3();
	private final Vector3 velocityFlock = new Vector3();
	private final Vector3 center = new Vector3();
	private final Vector3 localFlockvelocity = new Vector3();
	private final Vector3 chilling = new Vector3();

	public void move(Boid allboids[], int localNumboids, Vector3 localCenter,
			Vector3 localAverageVelocity, int totalNumboids,
			Vector3 flockCenter, Vector3 averageVelocity,
			Vector3[] localFlockCenter, int width, int height) {
		if (perching) {
			if (perch_timer > 0) {
				perch_timer--;
				return;
			} else {
				perching = false;
			}
		}

		// flockAvoidance.set(0, 0, 0);
		// for (int i = 0; i < localFlockCenter.length; i++) {
		// if (localFlockCenter[i] != localCenter) {
		// scaledPos.set(localFlockCenter[i]);
		// scaledPos.scl(1f / (float) localNumboids);
		// if (vecRdist(pos, scaledPos) <= 400) {
		// chill.set(pos);
		// chill.sub(scaledPos);
		// flockAvoidance.add(chill);
		// }
		// }
		// }
		// vector3RightShift(flockAvoidance, FLOCK_AVOIDANCE);
		//
		// perceiveCenter(centerFlock, flockCenter, pos, totalNumboids);
		// centerFlock.sub(pos);
		// vector3RightShift(centerFlock, FLOCK_CENTER_BIAS);

		averageVel(velocityFlock, averageVelocity, vel, totalNumboids);
		velocityFlock.sub(vel);
		vector3RightShift(velocityFlock, FLOCK_AVG_VEL);

		perceiveCenter(center, localCenter, pos, localNumboids);
		center.sub(pos);
		vector3RightShift(center, DEFAULT_CENTER_BIAS);

		averageVel(localFlockvelocity, localAverageVelocity, vel, localNumboids);
		localFlockvelocity.sub(vel);
		vector3RightShift(localFlockvelocity, DEFAULT_AVG_VEL);

		chillOut(allboids, localNumboids);
		vector3RightShift(chilling, DEFAULT_CHILLING);

		vel.add(center);
		vel.add(localFlockvelocity);
		vel.add(chilling);
		vel.add(velocityFlock);
		// vel.add(centerFlock);
		// vel.add(flockAvoidance);

		vector3Limit(vel, 100);

		pos.add(vel);

		if (upstroke) {
			if (wing_level >= 100) {
				upstroke = false;
			} else {
				wing_level += 40;
			}
		} else {
			if (wing_level <= -100) {
				upstroke = true;
			} else {
				wing_level -= 20;
			}

		}

		/* bound world */
		if (pos.x < -1800) {
			vel.x += 10;
		}
		if (pos.x > 1800) {
			vel.x -= 10;
		}
		if (pos.y < -1200) {
			vel.y += 10;
		}
		if (pos.y > 800) {
			vel.y -= 10;
		}
		if (pos.y > 1000) {
			pos.y = 1000; /* Hit ground!! */
			perching = true;
			perch_timer = rrand(20) + 30;
			wing_level = 60;
			vel.y = 0;
		}
		if (pos.z < 3000) {
			vel.z += 10;
		}
		if (pos.z > 5000) {
			vel.z -= 10;
		}

		perspective(width, height);
	}

	public void draw(ShapeRenderer shapeRenderer) {
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(Color.DARK_GRAY);

		shapeRenderer.triangle(shadow[0].x, shadow[0].y, shadow[1].x,
				shadow[1].y, shadow[2].x, shadow[2].y);
		shapeRenderer.triangle(shadow[0].x, shadow[0].y, shadow[1].x,
				shadow[1].y, shadow[3].x, shadow[3].y);

		lwing[0].x = X;
		lwing[1].x = tail_lX;
		lwing[2].x = tail_X;

		lwing[0].y = Y;
		lwing[1].y = tail_lY;
		lwing[2].y = tail_Y;

		rwing[0].x = X;
		rwing[1].x = tail_rX;
		rwing[2].x = tail_X;

		rwing[0].y = Y;
		rwing[1].y = tail_rY;
		rwing[2].y = tail_Y;
		/* if moving right => lwing behind rwing */
		if (vel.x > 0) {
			shapeRenderer.setColor(color);
			shapeRenderer.triangle(lwing[0].x, lwing[0].y, lwing[1].x,
					lwing[1].y, lwing[2].x, lwing[2].y);
			shapeRenderer.triangle(rwing[0].x, rwing[0].y, rwing[1].x,
					rwing[1].y, rwing[2].x, rwing[2].y);
			shapeRenderer.end();

			shapeRenderer.begin(ShapeType.Line);
			shapeRenderer.setColor(Color.BLACK);
			shapeRenderer.line(lwing[0].x, lwing[0].y, lwing[1].x, lwing[1].y);
			shapeRenderer.line(lwing[1].x, lwing[1].y, lwing[2].x, lwing[2].y);
			shapeRenderer.line(lwing[0].x, lwing[0].y, lwing[2].x, lwing[2].y);
			shapeRenderer.line(rwing[0].x, rwing[0].y, rwing[1].x, rwing[1].y);
			shapeRenderer.line(rwing[1].x, rwing[1].y, rwing[2].x, rwing[2].y);
			shapeRenderer.line(rwing[0].x, rwing[0].y, rwing[2].x, rwing[2].y);
			shapeRenderer.end();

		} else {

			shapeRenderer.setColor(color);
			shapeRenderer.triangle(rwing[0].x, rwing[0].y, rwing[1].x,
					rwing[1].y, rwing[2].x, rwing[2].y);
			shapeRenderer.triangle(lwing[0].x, lwing[0].y, lwing[1].x,
					lwing[1].y, lwing[2].x, lwing[2].y);
			shapeRenderer.end();

			shapeRenderer.begin(ShapeType.Line);
			shapeRenderer.setColor(Color.BLACK);
			shapeRenderer.line(rwing[0].x, rwing[0].y, rwing[1].x, rwing[1].y);
			shapeRenderer.line(rwing[1].x, rwing[1].y, rwing[2].x, rwing[2].y);
			shapeRenderer.line(rwing[0].x, rwing[0].y, rwing[2].x, rwing[2].y);
			shapeRenderer.line(lwing[0].x, lwing[0].y, lwing[1].x, lwing[1].y);
			shapeRenderer.line(lwing[1].x, lwing[1].y, lwing[2].x, lwing[2].y);
			shapeRenderer.line(lwing[0].x, lwing[0].y, lwing[2].x, lwing[2].y);
			shapeRenderer.end();
		}
	}

	public boolean isOnScreen(int W, int H) {
		return (X >= 0 && X < W && Y >= 0 && Y < H);
	}

	public boolean isOnScreen() {
		return onscreen;
	}

	private final Vector3 chill = new Vector3();

	public void chillOut(Boid boids[], int numboids) {
		chilling.set(0, 0, 0);
		for (int i = 0; i < numboids; i++) {
			if (boids[i] != this) {
				if (vecRdist(this.pos, boids[i].pos) <= 100) {
					chill.set(pos);
					chill.sub(boids[i].pos);
					chilling.add(chill);
				}
			}
		}
	}

	private static double vecRdist(Vector3 vec1, Vector3 vec2) {
		Vector3.X.set(vec1).sub(vec2);
		return Math.max(Math.max(Math.abs(Vector3.X.x), Math.abs(Vector3.X.y)),
				Math.abs(Vector3.X.z));
	}

	public static void perceiveCenter(Vector3 center, Vector3 realCent,
			Vector3 pos, int numboids) {
		center.set(realCent);
		center.sub(pos);
		center.scl(1.0f / ((float) (numboids - 1)));
	}

	public static void averageVel(Vector3 avgvelocity, Vector3 realAverageVel,
			Vector3 vel, int numboids) {
		avgvelocity.set(realAverageVel);
		avgvelocity.sub(vel);
		avgvelocity.scl(1.0f / ((float) (numboids - 1)));
	}

	private static void vector3RightShift(Vector3 vec, int n) {
		vec.x = ((int) vec.x) >> n;
		vec.y = ((int) vec.y) >> n;
		vec.z = ((int) vec.z) >> n;
	}

	/*
	 * Limit the length of the longest component to lim, while keeping others in
	 * proportion
	 */
	private static void vector3Limit(Vector3 vec, float lim) {
		float m, f;

		m = Math.max(Math.abs(vec.x), Math.abs(vec.y));
		m = Math.max(m, Math.abs(vec.z));

		if (m <= lim)
			return;

		f = lim / m;
		vec.sub(f);
	}

	/*
	 * Set the magnitude of the vector to a particular value
	 */
	private static void vector3SetMag(Vector3 vec, float mag) {
		float m, f;

		m = Math.max(Math.abs(vec.x), Math.abs(vec.y));
		m = Math.max(m, Math.abs(vec.z));

		f = mag / m;
		vec.scl(f);
	}

}
