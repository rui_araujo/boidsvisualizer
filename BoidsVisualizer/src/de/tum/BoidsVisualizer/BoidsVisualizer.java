package de.tum.BoidsVisualizer;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector3;

public class BoidsVisualizer implements ApplicationListener {
	private OrthographicCamera camera;

	private static int NUMBER_OF_BOIDS_FLOCK = 136;
	private static int NUMBER_OF_FLOCKS = 16;
	private final FPSLogger logger = new FPSLogger();
	private Boid[][] boids = new Boid[NUMBER_OF_FLOCKS][NUMBER_OF_BOIDS_FLOCK];
	private Color[] boidsColors = new Color[NUMBER_OF_FLOCKS];
	private Vector3[] localFlockCenter = new Vector3[NUMBER_OF_FLOCKS];
	private Vector3[] localAverageVelocity = new Vector3[NUMBER_OF_FLOCKS];
	private Vector3 centerFlock = new Vector3();
	private Vector3 averageVelocity = new Vector3();

	private ShapeRenderer shapeRenderer;
	private final Color grassColor = new Color(102 / 255.0f, 1, 51 / 255.0f, 1);
	private final Color blueSky = new Color(0, 204 / 255.0f, 1, 1);

	@Override
	public void create() {
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		camera = new OrthographicCamera();
		camera.setToOrtho(true);

		shapeRenderer = new ShapeRenderer();
		for (int i = 0; i < boidsColors.length; ++i) {
			final java.awt.Color c = java.awt.Color.getHSBColor(i
					* (1f / (float) boidsColors.length),// random
					// hue,
					// color
					1.0f,// full saturation, 1.0 for 'colorful' colors, 0.0 for
							// grey
					1.0f // 1.0 for bright, 0.0 for black
					);
			boidsColors[i] = new Color(c.getRed() / 255.0f,
					c.getGreen() / 255.0f, c.getBlue() / 255.0f, 1);
		}

		for (int i = 0; i < NUMBER_OF_FLOCKS; ++i) {
			localFlockCenter[i] = new Vector3();
			localAverageVelocity[i] = new Vector3();
			for (int j = 0; j < boids[i].length; ++j)
				boids[i][j] = new Boid((int) w, (int) h, boidsColors[i]);
		}
	}

	@Override
	public void dispose() {
		shapeRenderer.dispose();
	}

	@Override
	public void render() {
		logger.log();
		handleInput();
		shapeRenderer.setProjectionMatrix(camera.combined);
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(grassColor);
		shapeRenderer.triangle(0, Gdx.graphics.getHeight(), 0,
				Gdx.graphics.getHeight() / 2f, Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight() / 2f);
		shapeRenderer.triangle(0, Gdx.graphics.getHeight(),
				Gdx.graphics.getWidth(), Gdx.graphics.getHeight(),
				Gdx.graphics.getWidth(), Gdx.graphics.getHeight() / 2f);
		shapeRenderer.setColor(blueSky);
		shapeRenderer.triangle(0, 0, 0, Gdx.graphics.getHeight() / 2f,
				Gdx.graphics.getWidth(), 0);
		shapeRenderer.triangle(0, Gdx.graphics.getHeight() / 2f,
				Gdx.graphics.getWidth(), 0, Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight() / 2f);
		shapeRenderer.end();

		centerFlock.set(0, 0, 0);
		averageVelocity.set(0, 0, 0);
		for (int i = 0; i < NUMBER_OF_FLOCKS; ++i) {
			for (int j = 0; j < boids[i].length; ++j) {
				centerFlock.add(boids[i][j].pos);
				averageVelocity.add(boids[i][j].vel);
			}
		}

		for (int i = 0; i < NUMBER_OF_FLOCKS; ++i) {
			localFlockCenter[i].set(0, 0, 0);
			localAverageVelocity[i].set(0, 0, 0);

			for (int j = 0; j < boids[i].length; ++j) {
				localFlockCenter[i].add(boids[i][j].pos);
				localAverageVelocity[i].add(boids[i][j].vel);
			}
			for (int j = 0; j < boids[i].length; ++j) {

				boids[i][j].move(boids[i], boids[i].length,
						localFlockCenter[i], localAverageVelocity[i],
						NUMBER_OF_FLOCKS * NUMBER_OF_BOIDS_FLOCK, centerFlock,
						averageVelocity, localFlockCenter,
						(int) Gdx.graphics.getWidth(),
						(int) Gdx.graphics.getHeight());

				if (boids[i][j].isOnScreen()) {
					boids[i][j].draw(shapeRenderer);
				}
			}
		}
	}

	private final float rotationSpeed = 0.5f;

	private void handleInput() {
		if (Gdx.input.isKeyPressed(Input.Keys.A)) {
			camera.zoom += 0.02;
		}
		if (Gdx.input.isKeyPressed(Input.Keys.Q)) {
			camera.zoom -= 0.02;
		}
		if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
			if (camera.position.x > 0)
				camera.translate(-3, 0, 0);
		}
		if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
			if (camera.position.x < 1024)
				camera.translate(3, 0, 0);
		}
		if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
			if (camera.position.y > 0)
				camera.translate(0, -3, 0);
		}
		if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
			if (camera.position.y < 1024)
				camera.translate(0, 3, 0);
		}
		if (Gdx.input.isKeyPressed(Input.Keys.W)) {
			camera.rotate(-rotationSpeed, 0, 0, 1);
		}
		if (Gdx.input.isKeyPressed(Input.Keys.E)) {
			camera.rotate(rotationSpeed, 0, 0, 1);
		}
		camera.update();
	}

	@Override
	public void resize(int width, int height) {
		float aspectRatio = (float) width / (float) height;
		camera = new OrthographicCamera(2f * aspectRatio, 2f);
		camera.setToOrtho(true);
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

}
