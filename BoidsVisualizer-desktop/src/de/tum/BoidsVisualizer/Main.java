package de.tum.BoidsVisualizer;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "BoidsVisualizer";
		cfg.useGL20 = false;
		cfg.width = 1024;
		cfg.height = 768;
		cfg.backgroundFPS = 0;
		cfg.foregroundFPS = 30;
		new LwjglApplication(new BoidsVisualizer(), cfg);
	}
}
